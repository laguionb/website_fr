﻿#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


# ----------------- <CONTENT> -----------------

# AUTHOR = u'Duniter'
SITENAME = u'Duniter'
CC_LICENSE = "CC-BY-SA"
FAVICON = 'images/logos/Duniter-logo.svg'
SITELOGO = 'images/logos/Duniter-logo.svg'
THUMBNAIL_DEFAULT = 'images/logos/Duniter-logo.svg'
TIMEZONE = 'Europe/Paris'
SITELOGO_SIZE = 36
# BANNER = 'images/duniter-logo.png'
# BANNER_SUBTITLE = 'This is my subtitle'
FUNDING_PUBKEY = '78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8'
FUNDING_TITLE = 'Financement de ' + SITENAME + ' (ce mois-ci)'
FUNDING_BORDER_COLOR = 'ffffff'

USE_OPEN_GRAPH = True
OPEN_GRAPH_IMAGE = 'images/og_graph/duniter.png'

# ----------------- </CONTENT> -----------------







# ----------------- <THEME> -----------------

THEMES_PATH = "pelican-themes/"
THEME = THEMES_PATH + "pelican-bootstrap3"
CHILD_THEME_NAME = "pelican-bootstrap3-duniter"
CHILD_THEME = THEMES_PATH + CHILD_THEME_NAME + "/"

THEME_TEMPLATES_OVERRIDES = [
	CHILD_THEME + "templates", 
	'pelican-plugins/pelican-redirect/templates'
]

THEME_STATIC_PATHS = [
	'./static', 
	'../' + CHILD_THEME_NAME + '/static'
]

# EXTRA_TEMPLATES_PATHS = ['pelican-plugins/pelican-redirect/templates']

# ----------------- </THEME> -----------------












# ----------------- <URLS> -----------------

SEARCH_URL = SITEURL + '/search.html'

SLUGIFY_SOURCE = 'basename' # 'title'

ARTICLE_TRANSLATION_ID = 'slug'
PAGE_TRANSLATION_ID = 'slug'

PATH = 'content'
ARTICLE_PATHS = ['articles']
PAGE_PATHS = ['pages']
STATIC_PATHS = ['images', 'content', 'files', 'files/licence_g1.rst']
ARTICLE_EXCLUDES = ['files']
READERS = {'html': None} # Pour exclure les fichiers .html de la copie depuis les dossiers pages/ et articles/
INTRASITE_LINK_REGEX = '[{|](?P<what>.*?)[|}]'
FILENAME_METADATA = r'(?P<file_name>.*)'
PATH_METADATA = r'(?P<path>.*)'

BLOG_SLUG = 'blog'
IMPROVE_SITE_SLUG = 'contribuer/ameliorer-le-site'

STATIC_SAVE_AS = '{path}'

DIRECT_TEMPLATES = ['index', 'categories', 'authors', 'archives', 'search', 'tags']

if PRETTY_URLS :
	
	PAGE_SAVE_AS = '{slug}/index.html'
	PAGE_LANG_SAVE_AS = '{slug}-{lang}/index.html'
	INDEX_SAVE_AS = BLOG_SLUG + '/index.html'
	ARTICLE_SAVE_AS = BLOG_SLUG + '/{category}/{slug}/index.html'
	ARTICLE_LANG_SAVE_AS = BLOG_SLUG + '/{category}/{slug}-{lang}/index.html'
	CATEGORIES_SAVE_AS = BLOG_SLUG + '/categories/index.html'
	CATEGORY_SAVE_AS = BLOG_SLUG + '/{slug}/index.html'
	TAGS_SAVE_AS = BLOG_SLUG + '/etiquettes/index.html'
	TAG_SAVE_AS = BLOG_SLUG + '/etiquettes/{slug}/index.html'
	AUTHORS_SAVE_AS = BLOG_SLUG + '/auteurs/index.html'
	AUTHOR_SAVE_AS = BLOG_SLUG + '/auteurs/{slug}/index.html'
	REDIRECT_SAVE_AS = PAGE_SAVE_AS # Handled by a plugin

	PAGE_URL = '{slug}/'
	PAGE_LANG_URL = '{slug}-{lang}/'
	ARTICLE_URL = BLOG_SLUG + '/{category}/{slug}/'
	ARTICLE_LANG_URL = BLOG_SLUG + '/{category}/{slug}-{lang}/'
	CATEGORIES_URL = BLOG_SLUG + '/categories/'
	CATEGORY_URL = BLOG_SLUG + '/{slug}'
	TAGS_URL = BLOG_SLUG + '/etiquettes/'
	TAG_URL = BLOG_SLUG + '/etiquettes/{slug}/'
	AUTHORS_URL = BLOG_SLUG + '/auteurs/'
	AUTHOR_URL = BLOG_SLUG + '/auteur/{slug}/'

else : # For maximum compatibility (e.g. Apache servers without mod_rewrite)
	
	PAGE_SAVE_AS = '{slug}.html'
	PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'
	INDEX_SAVE_AS = BLOG_SLUG + '.html'
	ARTICLE_SAVE_AS = BLOG_SLUG + '/{category}/{slug}.html'
	ARTICLE_LANG_SAVE_AS = BLOG_SLUG + '/{category}/{slug}-{lang}.html'
	CATEGORIES_SAVE_AS = BLOG_SLUG + '/categories.html'
	CATEGORY_SAVE_AS = BLOG_SLUG + '/{slug}.html'
	TAGS_SAVE_AS = BLOG_SLUG + '/etiquettes.html'
	TAG_SAVE_AS = BLOG_SLUG + '/etiquettes/{slug}.html'
	AUTHORS_SAVE_AS = BLOG_SLUG + '/auteurs.html'
	AUTHOR_SAVE_AS = BLOG_SLUG + '/auteurs/{slug}.html'
	REDIRECT_SAVE_AS = PAGE_SAVE_AS # Handled by a plugin

	PAGE_URL = '{slug}.html'
	PAGE_LANG_URL = '{slug}-{lang}.html'
	ARTICLE_URL = BLOG_SLUG + '/{category}/{slug}.html'
	ARTICLE_LANG_URL = BLOG_SLUG + '/{category}/{slug}-{lang}.html'
	CATEGORIES_URL = BLOG_SLUG + '/categories.html'
	CATEGORY_URL = BLOG_SLUG + '/{slug}.html'
	TAGS_URL = BLOG_SLUG + '/etiquettes.html'
	TAG_URL = BLOG_SLUG + '/etiquettes/{slug}.html'
	AUTHORS_URL = BLOG_SLUG + '/auteurs.html'
	AUTHOR_URL = BLOG_SLUG + '/auteurs/{slug}.html'
	

BLOG_URL = PAGE_URL.replace('{slug}', BLOG_SLUG)
IMPROVE_SITE_URL = PAGE_URL.replace('{slug}', IMPROVE_SITE_SLUG)


# Documentation : 
# https://github.com/getpelican/pelican-plugins/tree/master/tag_cloud
TAG_CLOUD_SORTING = 'random'

BOOTSTRAP_THEME = 'readable'

PRIMARY_NAV = [
	# (label, slug)
	('<span class="optional">Une</span> monnaie libre', 'monnaies-libres'), 
	('<span class="optional">La </span>Ğ1', 'monnaie-libre-g1'), 
	('<span class="optional">La </span>Toile de Confiance', 'toile-de-confiance'), 
	('Contribuer', 'contribuer'), 
	('Forger<span class="optional"> des blocs</span>', 'miner-des-blocs'), 
	('Actualités', BLOG_SLUG), 
]

SECONDARY_NAV = [
	# (label, slug)
	( 'Licence',  'licences-du-site' ), 
	( 'Qui sommes-nous ?',  'qui-sommes-nous' ), 
	( 'Contact',  'contact' ), 
	( 'Financements',  'financements' )
]


SYNDICATION_LINKS = [
	# (CSS class, text/label, URL)
	( 'diaspora', 'Diaspora', 'https://framasphere.org/people/1949ee70f6bc0134e6b32a0000053625' ), 
	( 'mastodon',  'Mastodon', 'https://mastodon.xyz/@duniter' ), 
	( 'rss', 'RSS', SITEURL + '/feeds/all.atom.xml' ), 
]


# ----------------- </URLS> -----------------






# ----------------- <I18N> -----------------

I18N_SUBSITES = {
}

AVAILABLE_LANGS = ['fr', 'en']
DEFAULT_LANG = u'fr'


# ----------------- </I18N> -----------------






# ----------------- <PLUGINS> -----------------

PLUGIN_PATHS = ['pelican-plugins/', 'custom-plugins/']
PLUGINS = [
	'i18n_subsites', 'tipue_search', 'plantuml', 'assets', 'tag_cloud', 
	'pelican-page-hierarchy-duniter', 'pelican-linkclass']

ASSET_SOURCE_PATHS = [
    "../" + CHILD_THEME_NAME + '/static' + '/less'
    ]

MARKDOWN = {
  'extension_configs': {
    'markdown.extensions.codehilite': { 'css_class': 'highlight' },
    'markdown.extensions.fenced_code': {},
    'markdown.extensions.extra': {},
	  
	# Doc :
	# https://python-markdown.github.io/extensions/toc/
    'markdown.extensions.toc': { 
		'toc_depth' : '2-5', 
		'title' : 'Table des matières', 
		'permalink': True
	},
	  
    #'plantuml.plantuml_md': {'siteurl': '/fr'} DOES NOT WORK, NEED A PATCH ON THE PLUGIN PLANTUML, USE SITEURL BY DEFAULT
  }
}

PYGMENTS_STYLE = 'default'

JINJA_ENVIRONMENT = {
	'extensions': ['jinja2.ext.i18n']
}

# ----------------- </PLUGINS> -----------------







# ----------------- <DISPLAY> -----------------

BOOTSTRAP_FLUID = False

SHOW_ARTICLE_AUTHOR = True
SHOW_ARTICLE_AUTHOR = True
SHOW_ARTICLE_CATEGORY = False
SHOW_DATE_MODIFIED = True
DISPLAY_ARTICLE_INFO_ON_INDEX = False
DISPLAY_CATEGORY_IN_BREADCRUMBS = True

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False
DISPLAY_BREADCRUMBS = True

HIDE_SIDEBAR = True
DISPLAY_TAGS_ON_SIDEBAR = True
DISPLAY_CATEGORIES_ON_SIDEBAR = True
DISPLAY_AUTHORS_ON_SIDEBAR = True

DISPLAY_RECENT_POSTS_ON_SIDEBAR = True
RECENT_POST_COUNT = 3

DEFAULT_PAGINATION = 7
#PAGINATED_TEMPLATES = {'index': 7}

PAGES_SORT_ATTRIBUTE = 'order'

# ----------------- </DISPLAY> -----------------






# ----------------- <GENERATION> -----------------

import logging

LOG_FILTER = [
	#(logging.WARN, 'Empty alt attribute for image %s in %s'), 
	#(logging.WARN, '{filename} used for linking to static content %s in %s. Use {static} instead'),
	#(logging.WARN, "Meta tag in file %s does not have a 'name' attribute, skipping. Attributes: %s"), 
	# (logging.WARN, "Unable to find '%s', skipping url replacement."), 
]

# ----------------- </GENERATION> -----------------