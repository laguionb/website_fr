#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from jinja2 import Environment
import gettext

# ----------------- <URLS> -----------------
SITEURL = u'https://duniter.org/fr'
#RELATIVE_URLS = True # Comment this line if you don't want document-relative URLs
PRETTY_URLS = True
LAZY_LOAD = True

# ----------------- </URLS> -----------------

COMPILE_LESS_INTO_CSS = True

execfile('./commonconf.py')

