#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# ----------------- <URLS> -----------------

#SITEURL = u''
#SITEURL = u'http://localhost:8556'
#SITEURL = u'http://localhost:8000'
#SITEURL = u'https://duniter.borispaing.fr'
SITEURL = u'http://localhost/www/duniter/duniter_website_fr/output'
# RELATIVE_URLS = True # Comment this line if you don't want document-relative URLs
PRETTY_URLS = True
LAZY_LOAD = True


# ----------------- </URLS> -----------------

COMPILE_LESS_INTO_CSS = True




execfile('./commonconf.py')
