
function toggleMenu() {

  var menu = document.querySelector("nav#primaryMenu ul");

  var maxHeight = (menu.childElementCount * 59) + "px";

  if (menu.style.height != maxHeight) {

    menu.style.height = maxHeight;

  } else {

    menu.style.height = "0rem";

  }

}

document.querySelector("nav#primaryMenu button").addEventListener("click", toggleMenu);


$(document).ready(function() {

    var btn = $('#duniterProdVersion');
    var btnPre = $('#duniterTestVersion');
    var lnkPre = $('#duniterTestVersionLink');
    var initialButtonText = "Duniter ";

    // Latest pre-release
    $.get( "https://git.duniter.org/api/v4/projects/47/repository/tags", function( list ) {
		
        var max_tag = "";
        var max_tag_name = ""
		
        for (var i = 1; i < list.length; i++) {
			
            if (list[i].name.match(/^v?([0-9]+)\.([0-9]+)\.([0-9]+)/)) {
				
                let current_tag_name = list[i].name.match(/^v?([0-9]+)\.([0-9]+)\.([0-9]+)/)
				
                if (max_tag_name == "") {
					
                    max_tag = list[i].name
                    max_tag_name = current_tag_name
					
                } else if ( parseInt(current_tag_name[1]) > parseInt(max_tag_name[1])
                || ( parseInt(current_tag_name[1]) == parseInt(max_tag_name[1]) && parseInt(current_tag_name[2]) > parseInt(max_tag_name[2]) )
                || ( parseInt(current_tag_name[1]) == parseInt(max_tag_name[1]) && parseInt(current_tag_name[2]) == parseInt(max_tag_name[2]) && parseInt(current_tag_name[3]) > parseInt(max_tag_name[3]) )
                ) {
					
                    max_tag = list[i].name
                    max_tag_name = current_tag_name;
                }
            }
        }
		
        if (max_tag != "") {
			
            console.log('Test_version:', max_tag);
            btnPre.html(initialButtonText + ' ' + max_tag);
            lnkPre.attr('href', lnkPre.attr('href') + max_tag);
        }
		
    });
	
});


function copy() {
	
	var p = this.parent;
	var pubkeyAndCopyButton = p.parent;
	
	copyText.select();
	document.execCommand("copy");
	
	var successMsg = pubkeyAndCopyButton.querySelector(".successMsg");
	successMsg.style.opacity = "1";
	
	var copyButton = pubkeyAndCopyButton.querySelector(".copyButton");
	copyButton.style.animation = "none";
}

$(".pubkey-and-copy-button").each(function ( index ) {
	
	this.querySelector(".copyButton").addEventListener("click", copy);
	
});


/**
 * https://jsfiddle.net/ianclark001/aShQL/
 * ====
 * Check a href for an anchor. If exists, and in document, scroll to it.
 * If href argument ommited, assumes context (this) is HTML Element,
 * which will be the case when invoked by jQuery after an event
 */
function scroll_if_anchor(href) {
    href = typeof(href) == "string" ? href : $(this).attr("href");
    
    // You could easily calculate this dynamically if you prefer
    var fromTop = 0;
    
    // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
    // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
    if(href.indexOf("#") == 0) {
        var $target = $(href);
        
        // Older browser without pushState might flicker here, as they momentarily
        // jump to the wrong position (IE < 10)
        if($target.length) {
            $('html, body').animate({ scrollTop: $target.offset().top - fromTop });
            if(history && "pushState" in history) {
                history.pushState({}, document.title, window.location.pathname + href);
                return false;
            }
        }
    }
}    

// When our page loads, check to see if it contains and anchor
scroll_if_anchor(window.location.hash);

// Intercept all anchor clicks
document.querySelector("body").addEventListener("click", scroll_if_anchor);

